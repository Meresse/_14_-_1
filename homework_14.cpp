
#include <iostream>

using namespace std;

int main()
{
    setlocale(LC_ALL, "Russian");

    string str = "Проверка домашнего задания";

    cout << "Text : " << str << endl;
    cout << "Length : " << str.length() << endl;
    cout << "First symbol : " << str[0] << ", Last Symbol : " << str[str.length() - 1] << endl;
}